<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("Excel");

        $this->load->model("Student_model");
        $this->load->model("Section_model");
    }

    public function index()
    {
            // $info['file_name']  = $upload_data['file_name'];
            $info['file_name'] = 'soa.xlsx';

            $upload = $this->Student_model->uploadStudentBatchSOA($info);

            if($upload !== true)
            {
                $this->session->set_flashdata('error', 'File upload failed! Review the contents of the file and make sure you are following the correct format.');
                var_dump($this->db->last_query());
            }
    }
    public function index2()
    {
        $config['upload_path']          = FCPATH."/uploads/";
        $config['allowed_types']        = 'xls|xlsx';

        $this->load->library('upload', $config);
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $file_name = 'student_batch_upload_format.xlsx';
            $upload = $this->Student_model->uploadStudentBatch($file_name);

            if($upload !== true)
            {
                echo 'Failed to upload!';
                $this->session->set_flashdata('error', 'File upload failed! Review the contents of the file and make sure you are following the correct format.');
            }
            redirect('login/home', 'refresh');
    }

    public function getStudentGrades()
    {
        if(!empty($this->input->get('student_id'))) {
            $info['student_id'] = $this->input->get('student_id');
        } elseif(!empty($this->session->userdata('current_user_id'))) {
            $info['student_id'] = $this->session->userdata('current_user_id');
        }
        $student_grades = $this->Student_model->getStudentGrades($info['student_id']);

        if($student_grades != 0)
        {
            echo json_encode($student_grades);
        } else {
            echo '0';
        }
    }

    public function getStudentSOA($student_id)
    {
        $student_soa = $this->Student_model->getStudentSOA($student_id);

        echo json_encode($student_soa);
    }

    public function uploadStudentBatch()
    {
        $config['upload_path']          = FCPATH."/uploads/";
        $config['allowed_types']        = 'xls|xlsx';

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('student-batch-file'))
        {
            $error = array('error' => $this->upload->display_errors());
        }
        else
        {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $section_id = $this->input->post('section-list');
            $upload = $this->Student_model->uploadStudentBatch($file_name, $section_id);

            if($upload !== true)
            {
                $this->session->set_flashdata('error', 'File upload failed! Review the contents of the file and make sure you are following the correct format.');
            }
            redirect('login/home', 'refresh');
        }
    }

    public function uploadStudentBatchGrades()
    {
        $config['upload_path']          = FCPATH."/uploads/";
        $config['allowed_types']        = 'xls|xlsx';

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('student-batch-grades-file'))
        {
            $error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('error', 'File upload failed! Review the contents of the file and make sure you are following the correct format.');
            var_dump($error);
            return;
        }
        else
        {
            $upload_data = $this->upload->data();
            $info['file_name']  = $upload_data['file_name'];
            $info['section_id'] = $this->input->post('section-list');
            $info['quarter']    = $this->input->post('quarter-list');

            if($this->session->userdata('is_admin') == 1)
            {
                $info['faculty_id'] = $this->input->post('faculty_id');    
            } else {
                $info['faculty_id'] = $this->session->userdata('current_user_id');
            }

            $upload = $this->Student_model->uploadStudentBatchGrades($info);

            if($upload !== true)
            {
                $this->session->set_flashdata('error', 'File upload failed! Review the contents of the file and make sure you are following the correct format.');
            }
        }
        redirect('login/home', 'refresh');
    }

    public function uploadStudentBatchSOA()
    {
        $config['upload_path']          = FCPATH."/uploads/";
        $config['allowed_types']        = 'xls|xlsx';

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('student-batch-soa-file'))
        {
            $error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('error', 'File upload failed! Review the contents of the file and make sure you are following the correct format.');
            // var_dump($error);
            // return;
        }
        else
        {
            $upload_data = $this->upload->data();
            $info['file_name']  = $upload_data['file_name'];

            $upload = $this->Student_model->uploadStudentBatchSOA($info);

            if($upload !== true)
            {
                $this->session->set_flashdata('error', 'File upload failed! Review the contents of the file and make sure you are following the correct format.');
            }
        }
        redirect('login/loadSOAPage', 'refresh');
    }
}
