<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Section extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('current_user_id') != null)
        {
            $this->load->model("Section_model");
        } else {
            redirect('login', 'refresh');
        }
    }

    public function index()
    {
    }

    public function getSectionList()
    {
        $section_list = $this->Section_model->getSectionList();
        return $section_list;
    }

    public function addSection()
    {
        $info['grade_level'] = $this->input->post('grade_level');
        $info['section'] = $this->input->post('section');

        $add = $this->Section_model->addSection($info);
        if(!$add)
        {
            $this->session->set_flashdata('error', 'File upload failed! Review the contents of the file and make sure you are following the correct format.');
        }
        redirect('section/loadSectionPage', 'refresh');
    }

    public function deleteSection($section_id)
    {
        $info['section_id'] = $section_id;

        $delete = $this->Section_model->deleteSection($info);
        
        echo $delete;
    }

    public function updateSection()
    {
        $info['section_id'] = $this->input->post('section_id');
        $info['section_name'] = $this->input->post('section_name');
        $info['grade_level'] = $this->input->post('grade_level');

        $update = $this->Section_model->updateSection($info);
        echo $update;
    }

    public function loadSectionPage()
    {
        $info['section_list'] = $this->getSectionList();
        $this->template->load('main_template', 'section', $info);
    }

    public function loadAdvisoryArchivePage()
    {
        $info['advisory_list'] = $this->Advisory_model->getAdvisoryList();
        $this->template->load('main_template', 'advisory_archive', $info);
    }
}
