<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Faculty extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Faculty_model');
        $this->load->library("Excel");
    }

    public function index()
    {
        $file = FCPATH."/prerequisite/faculty_batch_upload_format.xlsx";

        $obj    = PHPExcel_IOFactory::load($file);
        $oSheet = $obj->getActiveSheet();
        $aCells = $obj->getActiveSheet()->getCellCollection();
        $start  = 2;
        var_dump($oSheet->getHighestDataRow());

        foreach($oSheet->getRowIterator($start) as $row)
        {
            // $row_number = $row->getRow();
            // var_dump($row);
            // var_dump($row_number);
            // var_dump($oSheet->getHighestDataColumn());
            foreach($row->getCellIterator() as $cell)
            {
                echo $cell;
                $highestRowColumn = $oSheet->getHighestDataColumn($cell->getRow());
                var_dump($highestRowColumn);
            }
            echo "<br>";
        }
    }

    public function getFacultyList()
    {
        $data['faculty_list'] = $this->Faculty_model->getFacultyList();
    }

    public function getFacultyIdBySectionId()
    {
        $info['section_id'] = (int)$this->input->get('section_id');
        $data['faculty_id'] = $this->Faculty_model->getFacultyIdBySectionId($info);
        echo json_encode($data['faculty_id'][0]);
        exit;
    }

    public function getSectionList()
    {
        $info['section_list'] = $this->Faculty_model->getSectionList();
        echo json_encode($info['section_list']);
    }

    public function getSectionListAllAndByFaculty()
    {
        $info['faculty_id'] = $this->input->get('faculty_id');
        $section_list = $this->Faculty_model->getSectionListAllAndByFaculty($info);
        echo json_encode($section_list);
    }

    public function getSectionListByFaculty()
    {
        $info['faculty_id'] = $this->input->get('faculty_id');
        $section_list = $this->Faculty_model->getSectionListByFaculty($info);
        echo json_encode($section_list);
    }

    public function updateSectionListByFaculty()
    {
        $info['section_list'] = json_decode($this->input->post('section_list'));
        $info['faculty_id'] = $this->input->post('faculty_id');

        $update = $this->Faculty_model->updateSectionListByFaculty($info);
        echo $update;
    }

    public function uploadFacultyBatch()
    {
        $config['upload_path']          = FCPATH."/uploads/";
        $config['allowed_types']        = 'xls|xlsx';

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('faculty-batch-file'))
        {
            $error = array('error' => $this->upload->display_errors());
        }
        else
        {
            $upload_data    = $this->upload->data();
            $file_name      = $upload_data['file_name'];
            $upload         = $this->Faculty_model->uploadFacultyBatch($file_name);

            if($upload !== true)
            {
                $this->session->set_flashdata('error', 'File upload failed! Review the contents of the file and make sure you are following the correct format.');
            }
            redirect('login/home', 'refresh');
        }
    }
}
