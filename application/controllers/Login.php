<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');

        if($this->session->userdata('current_user_id') != null)
        {
            $this->load->model('Faculty_model');
            $this->load->model('Student_model');
            $this->load->model('Section_model');
        }
    }

    function checkFacultyLogin()
    {
        $info['username'] = $this->input->post('form-username');
        $info['password'] = $this->input->post('form-password');
        // $info['username'] = "Cams";
        // $info['password'] = "hello";
        $iCheck = $this->Login_model->checkFacultyLogin($info);

        if(!$iCheck)
        {
            echo 'failed';
        } else {
            $this->session->set_userdata('current_user_id', $iCheck[0]);
            $this->session->set_userdata('current_user_type', 'Faculty');
            $this->session->set_userdata('is_admin', $iCheck[1]);
            // $this->template->load('main_template', 'home');
        }
    }

    function checkStudentLogin()
    {
        $info['username'] = $this->input->post('form-username');
        $info['password'] = $this->input->post('form-password');

        $iCheck = $this->Login_model->checkStudentLogin($info);

        if($iCheck !== false)
        {
            $this->session->set_userdata('current_user_id', $iCheck);
            $this->session->set_userdata('current_user_type', 'Student');
            // $this->template->load('main_template', 'home');
        } else {
            echo 'failed';
        }
    }

    function home()
    {
        if($this->session->userdata('current_user_type') === 'Student')
        {
            $this->loadStudentViewingPage();
        } else {
            if($this->session->userdata('is_admin') === "1")
            {
                $this->loadAccountManagementPage();
            } else {
                if($this->session->userdata('current_user_type') == 'Faculty')
                {
                    $this->loadGradebookPage();
                } else {
                    die($this->session->userdata('current_user_type'));
                    $this->loadStudentViewingPage();
                }
            }
        }
    }

    function loadAccountManagementPage()
    {
        if($this->session->userdata('current_user_type') == null && $this->session->userdata('is_admin') !== 1)
        {
            die("You are not authorized to access this page.");
        }
        $data = array();
        if($this->session->flashdata('error') !== null)
        {
            $data['error'] = $this->session->flashdata('error');
        }

        $data['faculty_list'] = $this->Faculty_model->getFacultyList();
        $data['student_list'] = $this->Student_model->getStudentList();
        $data['section_list'] = $this->Section_model->getSectionList();
        $this->template->load('main_template', 'account_management', $data);

        //Get section list
        // Load into sections into a select
        // 
    }

    function loadGradebookPage()
    {
        if($this->session->userdata('is_admin') == 1)
        {
            $data['student_list'] = $this->Student_model->getStudentList()  ;    
            $data['section_list'] = $this->Section_model->getSectionList();
            
        } else {
            $data['student_list'] = $this->Student_model->getStudentListByTeacher($this->session->userdata('current_user_id'));    
            $data['section_list'] = $this->Section_model->getSectionListByTeacher($this->session->userdata('current_user_id'));
        }
        $this->template->load('main_template', 'gradebook_page', $data);
    }

    function loadGradeInquiryPage()
    {
        $this->template->load('main_template', 'student_viewing');
    }

    function loadHomePage()
    {
        $this->template->load('main_template', 'home');
    }

    function loadFacultyLoginPage()
    {
        $this->load->view('faculty_login');
    }

    public function loadSOAPage()
    {
        $data['student_list'] = $this->Student_model->getStudentList();
        $this->template->load('main_template', 'SOA', $data);   
    }

    function loadStudentLoginPage()
    {
        $this->load->view('student_login');
    }

    public function loadStudentViewingPage()
    {
        $student_id = $this->session->userdata('current_user_id');
        if(!empty($student_id)) {
            $info['student_grades'] = $this->Student_model->getStudentGrades($student_id);
            $info['student_soa'] = $this->Student_model->getStudentSOA($student_id);
            $this->template->load('main_template', 'student_viewing', $info);
        }
    }

    function logout()
    {
        $this->session->sess_destroy();

        if($this->session->userdata('current_user_type') === 'Faculty' || $this->session->userdata('is_admin') === '1')
        {
            redirect('login_faculty', 'refresh');
        } else {
            redirect('login', 'refresh');
        }
        
    }
}
