<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('current_user_id') != null && $this->session->userdata('is_admin') == 1)
        {
            $this->load->model("User_model");
        }
    }

    public function index()
    {
    }

    public function deactivate($user_type, $user_id)
    {
        $update = $this->User_model->deactivate($user_type, $user_id);
        echo $update;
    }
    
}
