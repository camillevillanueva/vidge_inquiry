<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Advisory extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('current_user_id') != null)
        {
            $this->load->model("Advisory_model");
            $this->load->model("Student_model");
        } else {
            redirect('login', 'refresh');
        }
    }

    public function index()
    {
    }

    public function addAdvisory()
    {
        $info['user_id'] = $this->session->userdata('current_user_id');
        $info['message'] = $this->input->post('message');
        $info['title'] = $this->input->post('title');
        // $info['recipient_type'] = $this->input->post('recipient_type');
        $info['recipient'] = $this->input->post('student-recipient');

        $update = $this->Advisory_model->addAdvisory($info);
        if($update !== TRUE)
        {
            $this->session->set_flashdata('error', 'File upload failed! Review the contents of the file and make sure you are following the correct format.');
        } else {
            
            redirect('advisory/loadAddAdvisoryPage', 'refresh');
        }
    }

    public function deleteAdvisory()
    {
        $info['advisory_id'] = $this->input->post('advisory_id');
        $delete = $this->Advisory_model->deleteAdvisory($info);
        
        echo $delete;
    }

    public function getAllAdvisory()
    {
        $advisory = $this->Advisory_model->getAllAdvisory();
        echo json_encode($advisory);
    }

    public function getAdvisoryInformation($advisory_id)
    {
        if($this->session->userdata('current_user_type') === 'Student')
        {
            $info['current_student_id'] = $this->session->userdata('current_user_id');
            $info['advisory_id'] = $advisory_id;
            $update = $this->Advisory_model->updateAdvisoryReadStatus($info);
        }

        $get = $this->Advisory_model->getAdvisoryInformation($advisory_id);
        echo json_encode($get);
    }

    public function loadAddAdvisoryPage()
    {
        $info['student_list'] = $this->Student_model->getStudentList();
        $this->template->load('main_template', 'advisory_add', $info);
    }

    public function loadAdvisoryArchivePage()
    {
        $data = array();
        if($this->session->userdata('current_user_type') === 'Student')
        {
            $data['current_student_id'] = $this->session->userdata('current_user_id');    
        }
        
        $info['advisory_list'] = $this->Advisory_model->getAdvisoryList($data);
        $this->template->load('main_template', 'advisory_archive', $info);
    }
}
