<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Faculty</small></h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <form class="form-horizontal form-label-left input_mask" method="post" action="<?php echo base_url();?>Faculty/uploadFacultyBatch" enctype="multipart/form-data">
                        <div class="form-group form-faculty">
                            <table class="table table-striped table-bordered tbl-faculty" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Employee Code</th>
                                        <th>Last Name</th>
                                        <th>First Name</th>
                                        <th>Middle Name</th>
                                        <th>Username</th>
                                        <th>Administrator</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if($faculty_list !== 0)
                                    {
                                        foreach($faculty_list as $teacher)
                                        {
                                            ?>
                                            <tr class="faculty" id="<?php echo $teacher->code;?>">
                                                <td><?php echo $teacher->code;?></td>
                                                <td><?php echo $teacher->last_name;?></td>
                                                <td><?php echo $teacher->first_name;?></td>
                                                <td><?php echo $teacher->middle_name;?></td>
                                                <td><?php echo $teacher->username;?></td>
                                                <td><?php echo $teacher->is_admin === '1' ? 'Yes' : 'No';?></td>
                                                <td><button type="button" class="btn btn-danger deactivate" id='<?php echo $teacher->id;?>' data-type='faculty'>Deactivate</button></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="x_title">
                            <h2>File Upload</small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <label class="btn btn-primary btn-upload" for="faculty-batch-file" title="Upload .XLS/.XLSX file">
                                <input type="file" class="sr-only" id="faculty-batch-file" name="faculty-batch-file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                                <span class="docs-tooltip" data-toggle="tooltip" title="Upload .XLS/.XLSX file" data-original-title="Upload .XLS/.XLSX file">
                                    <span class="fa fa-upload"></span>
                                </span>
                            </label>
                            <button type="submit" class="btn btn-success">Upload</button>
                        </div>
                    </form>
                    <?php
                    if(isset($error))
                    {
                        ?>
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong><?php echo $error; ?></strong>
                        </div>
                        <?php
                        $error = '';
                    }
                    ?>
                </div>
            </div> <!-- End of Panel -->
        </div> <!-- End of column -->
    </div> <!-- End of Faculty Row -->

    <!-- Student Row -->
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Students </h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="x_content">
                        <form class="form-horizontal form-label-left input_mask" method="post" action="<?php echo base_url();?>Student/uploadStudentBatch" enctype="multipart/form-data">
                            <div class="form-group">
                                <table class="table table-striped table-bordered tbl-student" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Student ID</th>
                                            <th>Last Name</th>
                                            <th>First Name</th>
                                            <th>Middle Initial</th>
                                            <th>Username</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if($student_list !== 0)
                                        {
                                            foreach($student_list as $student)
                                            {
                                                ?>
                                                <tr class="student">
                                                    <td><?php echo $student->student_id;?></td>
                                                    <td><?php echo $student->last_name;?></td>
                                                    <td><?php echo $student->first_name;?></td>
                                                    <td><?php echo $student->middle_name;?></td>
                                                    <td><?php echo $student->username;?></td>
                                                    <td><button type="button" class="btn btn-danger deactivate" id='<?php echo $student->id;?>' data-type='student'>Deactivate</button></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="x_title">
                                <h2>File Upload</small></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                    <label class="control-label col-md-1 col-sm-3 col-xs-12">Section</label>
                                    <div class="col-md-11 col-sm-11 col-xs-12">
                                      <select class="form-control section-list" name="section-list">
                                        <?php
                                        if($section_list != 0)
                                        {
                                            foreach($section_list as $section)
                                            {
                                              ?>
                                              <option value="<?php echo $section->id;?>"><?php echo $section->section;?></option>
                                              <?php
                                            }
                                      }
                                      ?>
                                    </select>
                              </div>
                          </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <label class="btn btn-primary btn-upload" for="student-batch-file" title="Upload .XLS/.XLSX file">
                                    <input type="file" class="sr-only" id="student-batch-file" name="student-batch-file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                                    <span class="docs-tooltip" data-toggle="tooltip" title="Upload .XLS/.XLSX file" data-original-title="Upload .XLS/.XLSX file">
                                        <span class="fa fa-upload"></span>
                                    </span>

                                </label>
                                <button type="submit" class="btn btn-success">Upload</button>
                            </div>
                        </form>
                        <?php
                        if(isset($error))
                        {
                            ?>
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong><?php echo $error; ?></strong>
                            </div>
                            <?php
                            $error = '';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Student Row -->

    <!-- Modal - Handled Sections for Faculty -->
    <div class="modal fade modal-faculty-handled-sections" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Handled Sections</h4>
                </div>

                <form id="form-modal-faculty-handled-sections" data-parsley-validate class="form-horizontal form-label-left">
                    <div class="modal-body">
                        <div class="form-group">
                            <table class="table table-striped table-bordered tbl-sections" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Grade Level</th>
                                        <th>Section</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-success update-faculty-sections">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!-- /Modal - Handled Sections for Faculty-->
</div>
