<div class="right_col" role="main">

    <div class="row">    <!-- SOA Row -->

        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Statement of Account </h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="x_content">
                        <form class="form-horizontal form-label-left input_mask">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#tab-grades" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Grades</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#tab-soa" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Statement of Account</a>
                                    </li>
                                </ul>
                                <div id="student-tabs" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="tab-grades" aria-labelledby="home-tab">
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table class="table table-striped table-bordered tbl-modal-student-grades" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Subject</th>
                                                            <th>Q1</th>
                                                            <th>Q2</th>
                                                            <th>Q3</th>
                                                            <th>Q4</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        if($student_grades !== 0)
                                                        {
                                                            foreach($student_grades as $grade)
                                                            {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $grade->subject;?></td>
                                                                    <td><?php echo $grade->Q1;?></td>
                                                                    <td><?php echo $grade->Q2;?></td>
                                                                    <td><?php echo $grade->Q3;?></td>
                                                                    <td><?php echo $grade->Q4;?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab-soa" aria-labelledby="profile-tab">
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table class="table table-striped table-bordered tbl-modal-student-soa" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Fee</th>
                                                            <th>Amount Due</th>
                                                            <th>Due Date</th>
                                                            <th>Amount Paid</th>
                                                            <th>Payment Date</th>
                                                            <th>Balance</th>
                                                            <th>Remarks</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        if($student_soa !== 0 && $student_soa !== null)
                                                        {
                                                            foreach($student_soa as $soa)
                                                            {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $soa->fee;?></td>
                                                                    <td><?php echo $soa->amount;?></td>
                                                                    <td><?php echo $soa->due_date;?></td>
                                                                    <td><?php echo $soa->amount_paid;?></td>
                                                                    <td><?php echo $soa->payment_date;?></td>
                                                                    <td><?php echo number_format(($soa->amount - $soa->amount_paid),2);?></td>
                                                                    <td><?php echo $soa->remarks;?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Student Row -->
</div>

