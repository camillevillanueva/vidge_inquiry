<div class="right_col" role="main">

    <!-- Advisory Row -->
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Advisory History</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="x_content">
                        <form class="form-horizontal form-label-left input_mask advisory">

                            <div class="form-group">
                                <table class="table table-striped table-bordered tbl-advisory-list" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Title</th>
                                            <th>Published Date</th>
                                            <th>Author</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if($advisory_list !== 0)
                                        {
                                            foreach($advisory_list as $advisory)
                                            {
                                                ?>
                                                <tr class="advisory" id="<?php echo $advisory->id;?>" <?php if($this->session->userdata('current_user_type') === "Student" && $advisory->is_read === '0'){
                                                    ?>
                                                    style="background-color: #1ABB9C; color: #fff"
                                                    <?php
                                                }?>>
                                                    <td><?php echo $advisory->status;?></td>
                                                    <td><?php echo $advisory->title;?></td>
                                                    <td><?php echo $advisory->published_date;?></td>
                                                    <td><?php echo $advisory->author;?></td>
                                                </tr>   
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php
                        if(isset($error))
                        {
                            ?>
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong><?php echo $error; ?></strong>
                            </div>
                            <?php
                            $error = '';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Archive Row -->

<!-- Advisory Info Modal -->

<div class="modal fade modal-advisory-information" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Advisory</h4>
            </div>

            <form id="form-modal-advisory-information" data-parsley-validate class="form-horizontal form-label-left">
                <div class="modal-body">
                    <div class="mail_list">
                        <div class="left">
                        </div>
                        <div class="right">
                            <h3>Title: </h3>
                            <p id="advisory-title"></p>
                        </div>
                    </div>
                    <div class="mail_list">
                        <div class="left">
                        </div>
                        <div class="right">
                            <h3>Message: </h3>
                            <p id="advisory-message"></p>
                        </div>
                    </div>
                    <div class="mail_list">
                        <div class="left">
                        </div>
                        <div class="right">
                            <h3>Published Date: </h3>
                            <p id="advisory-published-date"></p>
                        </div>
                    </div>
                    <div class="mail_list">
                        <div class="left">
                        </div>
                        <div class="right">
                            <h3>Recipients: </h3>
                            <p id="advisory-recipients"></p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <?php if ($this->session->userdata('current_user_type') != 'Student')
                    {
                        ?>
                        <button type="button" class="btn btn-danger btn-delete-advisory" data-dismiss="modal">Delete Advisory</button>
                        <?php
                    }
                    ?>
                </div>
            </form>

        </div>
    </div>
</div>
