<div class="right_col" role="main">

    <!-- Section Row -->
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Sections </h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="x_content">
                        <form class="form-horizontal form-label-left input_mask" method="post" action="<?php echo base_url();?>Section/addSection">
                            <div class="form-group">
                                <table class="table table-striped table-bordered tbl-sections" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Grade Level</th>
                                            <th>Section</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if($section_list !== 0)
                                        {
                                            foreach($section_list as $section)
                                            {
                                                ?>
                                                <tr class="section" id="<?php echo $section->id;?>">
                                                    <td class="grade-level"><?php echo $section->grade_level;?></td>
                                                    <td class="section-name"><?php echo $section->name;?></td>
                                                    <td><button type="button" class="btn btn-danger delete-section" id='<?php echo $section->id;?>'>Archive</button></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="x_title">
                                <h2>Add a Section</small></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-1 col-sm-3 col-xs-12">Grade Level</label>
                                <div class="col-md-11 col-sm-11 col-xs-12">
                                    <input type="text" class="form-control" name="grade_level" placeholder="Grade Level (Required)" required="required">
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="control-label col-md-1 col-sm-3 col-xs-12">Section</label>
                                <div class="col-md-11 col-sm-11 col-xs-12">
                                    <input type="text" class="form-control" name="section" placeholder="Section Name (Required)" required="required">
                                </div>
                            </div>  
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                        <?php
                        if(isset($error))
                        {
                            ?>
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong><?php echo $error; ?></strong>
                            </div>
                            <?php
                            $error = '';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Section Row -->
</div>

<div class="modal fade modal-section-list" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Update Section</h4>
            </div>

            <form id="form-modal-section-list" data-parsley-validate class="form-horizontal form-label-left">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-md-1 col-sm-3 col-xs-12">Grade Level</label>
                        <div class="col-md-11 col-sm-11 col-xs-12">
                            <input type="text" class="form-control" name="grade_level" id="grade-level" placeholder="Grade Level (Required)" required="required">
                        </div>
                    </div>  
                    <div class="form-group">
                        <label class="control-label col-md-1 col-sm-3 col-xs-12">Section</label>
                        <div class="col-md-11 col-sm-11 col-xs-12">
                            <input type="text" class="form-control" name="section" id="section-name" placeholder="Section Name (Required)" required="required">
                        </div>
                    </div>  
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success update-section">Update</button>
                    <button type="button" class="btn btn-default btn-close-receipt" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
