<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
          
        <title>Vidge </title>

        <link href="<?php echo base_url(); ?>assets/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type='text/css'>

        <!-- AutoComplete -->
        <!-- <link href="<?php //echo base_url(); ?>assets/js/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet" type='text/css'> -->

        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo base_url(); ?>assets/template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="<?php echo base_url(); ?>assets/template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="<?php echo base_url(); ?>assets/template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- bootstrap-wysiwyg -->
        <link href="<?php echo base_url(); ?>assets/template/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="<?php echo base_url(); ?>assets/template/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
        <!-- Switchery -->
        <link href="<?php echo base_url(); ?>assets/template/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
        <!-- starrr -->
        <link href="<?php echo base_url(); ?>assets/template/vendors/starrr/dist/starrr.css" rel="stylesheet">
        <!-- bootstrap-daterangepicker -->
        <link href="<?php echo base_url(); ?>assets/template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="<?php echo base_url(); ?>assets/template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md">

        <div class="container body">
            <div class="main_container">

                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                          <a href="" class="site_title"><i class="fa fa-apple"></i> <span>Vidge Inquiry</span></a>
                        </div>
                        <!-- menu profile quick info -->
                        <div class="profile clearfix">                            
                            <div class="profile_info">
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>General</h3>
                                <ul class="nav side-menu">
                                        <li><a><i class="fa fa-home"></i> Advisory <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <?php
                                                if($this->session->userdata('current_user_type') === 'Faculty')
                                                {
                                                    ?>
                                                <li><a href="<?php echo base_url();?>advisory">New Advisory</a></li>
                                                <li><a href="<?php echo base_url();?>advisory/loadAdvisoryArchivePage">List of Advisories</a></li>
                                            </ul>
                                        </li>
                                        <?php
                                        if($this->session->userdata('is_admin') === '1')
                                        {
                                            ?>
                                            <li><a href="<?php echo base_url();?>accounts"><i class="fa fa-home"></i> Account Management </a></li>
                                            <?php
                                        }
                                    ?>
                                    <?php
                                        if($this->session->userdata('is_admin') === '1')
                                        {
                                            ?>
                                            <li><a href="<?php echo base_url();?>sections"><i class="fa fa-home"></i> Section Management </a></li>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <li><a href="<?php echo base_url();?>advisory_list">List of Advisories</a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                            <div class="menu_section">
                                <h3>Inquiry</h3>
                                <ul class="nav side-menu">
                                    <?php
                                    if($this->session->userdata('current_user_type') === 'Faculty')
                                    {
                                        ?>
                                        <?php
                                        if($this->session->userdata('is_admin') === '1')
                                        {
                                            ?>
                                            <li><a href="<?php echo base_url();?>login/loadSOAPage"><i class="fa fa-home"></i> Statement of Account</a></li>
                                            <?php
                                        }
                                        ?>
                                        <li><a href="<?php echo base_url();?>gradebook"><i class="fa fa-edit"></i> Gradebook</a>
                                        </li>
                                        <?php
                                    } else {
                                        ?>
                                        <li><a href="<?php echo base_url();?>home"><i class="fa fa-home"></i> Grades/Statement of Account </a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <!-- /sidebar menu -->
                        
                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url();?>Login/logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            </a>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav ">
                    <div class="nav_menu hide-top-nav">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li id="logout"><a href="<?php echo base_url();?>/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->

                <!-- Content Area -->
                <div id="content-loader"><?php echo $body; ?></div>

            </div>
        </div>
        <script>
            var base_url = "<?php echo base_url();?>";
        </script>
        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/nprogress/nprogress.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/iCheck/icheck.min.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/moment/min/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- bootstrap-wysiwyg -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/vendors/google-code-prettify/src/prettify.js"></script>
        <!-- jQuery Tags Input -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
        <!-- Switchery -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/switchery/dist/switchery.min.js"></script>
        <!-- Select2 -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/select2/dist/js/select2.full.min.js"></script>
        <!-- Autosize -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/autosize/dist/autosize.min.js"></script>
        <!-- jQuery autocomplete -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
        <!-- starrr -->
        <script src="<?php echo base_url(); ?>assets/template/vendors/starrr/dist/starrr.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="<?php echo base_url(); ?>assets/template/build/js/custom.min.js"></script>

        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>assets/DataTables/DataTables-1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.min.js"></script>

        <!-- SELF SCRIPTS -->

        <script src="<?php echo base_url(); ?>assets/js/gradebook.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/account_management.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/advisory.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/soa.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/section.js"></script>
    </body>
</html>
