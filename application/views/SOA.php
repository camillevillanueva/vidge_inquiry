<div class="right_col" role="main">

    <!-- SOA Row -->
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Statement of Account </h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <br />
                    <div class="x_content">
                        <br />    
                        <form class="form-horizontal form-label-left input_mask" method="post" action="<?php echo base_url();?>Student/uploadStudentBatchSOA" enctype="multipart/form-data">
                            <div class="form-group">
                                <table class="table table-striped table-bordered tbl-student-soa" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Student ID</th>
                                            <th>Grade/Section</th>
                                            <th>Full Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if($student_list !== 0)
                                        {
                                            foreach($student_list as $student)
                                            {
                                                ?>
                                                <tr class="student" id="<?php echo $student->student_id;?>">
                                                    <td><?php echo $student->student_id;?></td>
                                                    <td><?php echo $student->grade_section;?></td>
                                                    <td><?php echo $student->full_name;?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="x_title">
                                <h2>File Upload</small></h2>
                                <div class="clearfix"></div>
                            </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <label class="btn btn-primary btn-upload" for="student-batch-soa-file" title="Upload .XLS/.XLSX file">
                            <input type="file" class="sr-only" id="student-batch-soa-file" name="student-batch-soa-file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                            <span class="docs-tooltip" data-toggle="tooltip" title="Upload .XLS/.XLSX file" data-original-title="Upload .XLS/.XLSX file">
                                <span class="fa fa-upload"></span>
                            </span>

                        </label>
                        <button type="submit" class="btn btn-success">Upload</button>
                    </div>
                </form>
                <?php
                if(isset($error))
                {
                    ?>
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $error; ?></strong>
                    </div>
                    <?php
                    $error = '';
                }
                ?>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /Student Row -->
</div>

<div class="modal fade modal-student-soa" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Grades</h4>
            </div>

            <form id="form-modal-student-soa" data-parsley-validate class="form-horizontal form-label-left">
                <div class="modal-body">
                    <div class="mail_list">
                        <div class="left">
                        </div>
                        <div class="right">
                            <h3>Student ID: </h3>
                            <p id="student-id"></p>
                        </div>
                    </div>
                    <div class="mail_list">
                        <div class="left">
                        </div>
                        <div class="right">
                            <h3>Full Name: </h3>
                            <p id="student-full-name"></p>
                        </div>
                    </div>
                    <div class="mail_list">
                        <div class="left">
                        </div>
                        <div class="right">
                            <h3>Grade/Section: </h3>
                            <p id="student-grade-section"></p>
                        </div>
                    </div>
                    <br/>
                    <br/>

                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <table class="table table-striped table-bordered tbl-modal-student-soa" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Fee</th>
                                        <th>Amount Due</th>
                                        <th>Due Date</th>
                                        <th>Amount Paid</th>
                                        <th>Payment Date</th>
                                        <th>Balance</th>
                                        <th>Remarks</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-close-receipt" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
