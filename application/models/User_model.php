<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function deactivate($user_type, $user_id)
    {
        $this->db->set('is_archived', '1', FALSE);
        $this->db->where('id', $user_id);

        if($user_type == 'student')
        {
            $this->db->update('student');
        } else {
            $this->db->update('faculty');
        }

        if ($this->db->_error_message())
        {
            return 0;
        }
    }
}
