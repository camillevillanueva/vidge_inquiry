<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Advisory_model extends CI_Model
{
    public function addAdvisory($info)
    {
        $sql_array = array();

        // if($this->input->post('recipient_type') == 'all')
        // {
        //     array_push($sql_array, array(
        //                                 'author_id' => $info['user_id'],
        //                                 'message' => $info['message'],
        //                                 'recipient_id' => $info['recipient'],
        //                                 'recipient_type' => 'all'
        //                             )
        //             );
        // } else if($this->input->post('recipient_type') == 'grade')
        // {
        //     array_push($sql_array, array(
        //                                 'author_id' => $info['user_id'],
        //                                 'message' => $info['message'],
        //                                 'recipient_id' => $info['recipient'],
        //                                 'recipient_type' => 'grade',
        //                                 'grade_level' => $info['recipient']
        //                             )
        //             );
        // } else if($this->input->post('recipient_type') == 'section')
        // {
        //     array_push($sql_array, array(
        //                                 'author_id' => $info['user_id'],
        //                                 'message' => $info['message'],
        //                                 'recipient_id' => $info['recipient'],
        //                                 'recipient_type' => 'section',
        //                                 'section_id' => $info['recipient']
        //                             )
        //             );
            
        // } else if($this->input->post('recipient_type') == 'student')
        // {
        //     array_push($sql_array, array(
        //                                 'author_id' => $info['user_id'],
        //                                 'message' => $info['message'],
        //                                 'recipient_id' => $info['recipient'],
        //                                 'recipient_type' => 'student',
        //                                 'student_id' => $info['recipient']
        //                             )
        //             );
        // }
        
        $sql_array = array(
                        'author_id' => $info['user_id'],
                        'title' => $info['title'],
                        'message' => $info['message'],
                        );
        if(!$this->db->insert('advisory', $sql_array))
        {
            $error = $this->db->error();
            return $error['message'];
        } else {
            $advisory_id = $this->db->insert_id();
            $sql_array = array();
            foreach($info['recipient'] as $recipient_id)
            {
                array_push($sql_array, array(
                            'advisory_id' => $advisory_id,
                            'student_id' => $recipient_id
                        ));
            }

            if(!$this->db->insert_batch('advisory_recipient', $sql_array))
            {
                $error = $this->db->error();
                return $error['message'];
            } else {
                return TRUE;
            }
        }
    }

    public function deleteAdvisory($info)
    {
        $sql_array = array(
                        'is_archived' => 1
                        );
        $this->db->set('is_archived', '1', FALSE);
        $this->db->where('id', $info['advisory_id']);

        if(!$this->db->update('advisory'))
        {
            return 0;
        }
    }

    public function getAdvisoryInformation($advisory_id)
    {
        $this->db->select('a.published_date, 
                            a.message, 
                            a.title, 
                            CONCAT(f.first_name, " ", f.last_name) AS author, 
                            CONCAT(s.first_name, " ", s.last_name) AS recipient,
                            a.id');
        $this->db->from('advisory AS a');
        $this->db->join('advisory_recipient AS af', 'a.id = af.advisory_id');
        $this->db->join('student AS s', 'af.student_id = s.student_id');
        $this->db->join('faculty AS f', 'a.author_id = f.code');
        $this->db->where('a.id', $advisory_id);

        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function getAdvisoryList($info = null)
    {
        $this->db->select('a.published_date, 
                            a.title, 
                            CONCAT(f.first_name, " ", f.last_name) AS author, 
                            a.id, 
                            (CASE 
                                WHEN a.is_archived = 1 
                                THEN "Archived"
                                ELSE "Published" 
                            END) AS status');

        $this->db->from('advisory AS a');
        $this->db->join('faculty AS f', 'a.author_id = f.code');

        if($info != null)
        {
            if($info['current_student_id'] != null)
            {
                $this->db->select('ar.is_read');
                $this->db->join('advisory_recipient AS ar', 'a.id = ar.advisory_id');
                $this->db->join('student AS s', 'ar.student_id = s.student_id');
                $this->db->where('s.student_id = ', $info['current_student_id']);
                $this->db->where('a.is_archived','0');
            }
        }
        $this->db->order_by('published_date', 'desc');
        $query = $this->db->get();
        // var_dump($this->db->last_query());

        if($query->num_rows() > 0)
        {
            return $query->result();
        } return 0;
    }

    public function updateAdvisoryReadStatus($info)
    {
        $this->db->set('is_read', '1');
        $this->db->where('student_id = '.$this->db->escape($info['current_student_id']).' AND advisory_id = '.$this->db->escape($info['advisory_id']));
        $this->db->update('advisory_recipient');
    }
}
