<?php   defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model 
{
    function checkFacultyLogin($info)
    {
        $sql_array = array(
                        $info['username'],
                        $info['password']
                    );

        $sql = "SELECT
                    code,
                    password,
                    is_admin
                FROM
                    faculty
                WHERE
                    username = ?
                AND
                    is_archived = 0
                LIMIT
                    1";

        $query = $this->db->query($sql, $info['username']);

        if($query->num_rows() > 0)
        {
            $stored_password = $query->row()->password;
            $current_user_id = $query->row()->code;
            $is_admin        = $query->row()->is_admin;

            if(password_verify($info['password'], $stored_password))
            {
                return array($current_user_id, $is_admin);
            }
        } 
        return false;
    }

    function checkStudentLogin($info)
    {
        $sql_array = array(
                        $info['username'],
                        $info['password']
                    );

        $sql = "SELECT
                    student_id,
                    password
                FROM
                    student
                WHERE
                    username = ?
                AND
                    is_archived = 0
                LIMIT
                    1";

        $query = $this->db->query($sql, $info['username']);

        if($query->num_rows() > 0)
        {
            $stored_password = $query->row()->password;
            $current_user_id = $query->row()->student_id;

            if(password_verify($info['password'], $stored_password))
            {
                return $current_user_id;
            }
        } 
        return false;
    }
}
