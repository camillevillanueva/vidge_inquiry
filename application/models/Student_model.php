<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_model extends CI_Model
{
    public function getStudentList()
    {
        $sql = "SELECT
                    *,
                    CONCAT(s.last_name, ', ', s.first_name, ' ', s.middle_name) As full_name,
                    CONCAT('Grade ', se.grade_level, ' - ', se.name) As grade_section
                FROM
                    student AS s
                INNER JOIN
                    section AS se
                ON
                    s.grade = se.grade_level
                    AND
                    s.section = se.name
                WHERE
                    s.is_archived = 0";
        $query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            return $query->result();
        } else 
        {
            return 0;
        }
    }

    public function getStudentGrades($student_id)
    {
        $sql = "SELECT
                    sg.student_id As student_id_autoinc,  
                    s.student_id,
                    sg.subject,
                    CASE WHEN (quarter=1) THEN sg.grade ELSE 0 END AS Q1,
                    CASE WHEN (quarter=2) THEN sg.grade ELSE 0 END AS Q2,
                    CASE WHEN (quarter=3) THEN sg.grade ELSE 0 END AS Q3,
                    CASE WHEN (quarter=4) THEN sg.grade ELSE 0 END AS Q4,
                    CONCAT(s.last_name, ', ', s.first_name, ' ', s.middle_name) As full_name,
                    CONCAT('Grade ', se.grade_level, ' - ', se.name) As grade_section
                FROM 
                    student_grade As sg
                INNER JOIN
                    student As s
                ON
                    sg.student_id = s.student_id
                INNER JOIN
                    section As se
                ON
                    (s.grade = se.grade_level
                    AND
                    s.section = se.name)
                WHERE
                    sg.student_id = '" . $student_id . "'" . "
                GROUP BY 
                    subject";
        $query = $this->db->query($sql, $student_id);

        if($query->num_rows() > 0)
        {
            return $query->result();
        } return 0;
    }

    public function getStudentListByTeacher($teacher_id)
    {
        $sql = "SELECT
                    s.student_id,
                    CONCAT(s.last_name, ', ', s.first_name, ' ', s.middle_name) As full_name,
                    CONCAT('Grade ', grade_level, ' - ', se.name) As grade_section

                FROM
                    student As s
                INNER JOIN
                    (SELECT 
                        id, grade_level, name
                    FROM
                        section) AS se
                ON
                    s.grade = se.grade_level
                    AND
                    s.section = se.name
                INNER JOIN
                    faculty_section As fs
                ON
                    fs.section_id = se.id
                WHERE
                    fs.faculty_id = ?
                AND
                    s.is_archived = 0";
        $query = $this->db->query($sql, $teacher_id);

        if($query->num_rows() > 0)
        {
            return $query->result();
        } else 
        {
            return 0;
        }
    }

    public function getStudentSOA($student_id)
    {
        $this->db->select('soa.*,
                            s.student_id,
                            CONCAT(s.first_name, " ", s.middle_name, " ", s.last_name) AS full_name,
                            CONCAT(se.grade_level, " - ", se.name) As grade_section');
        $this->db->from('student_soa AS soa');
        $this->db->join('student AS s', 'soa.student_id = s.student_id');
        $this->db->join('section AS se', 's.grade = se.grade_level AND s.section = se.name');
        $this->db->where('s.student_id', $student_id);
        $this->db->where('soa.is_archived', 'n');
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        }
    }

    public function uploadStudentBatch($file_name, $section_id)
    {
        $file = FCPATH."uploads\\".$file_name;

        $obj    = PHPExcel_IOFactory::load($file);
        $oSheet = $obj->getActiveSheet();
        $aCells = $obj->getActiveSheet()->getCellCollection();
        $start  = 2;
        
        $sql = "DELETE FROM student";
        $query = $this->db->query($sql);
        
        if(!$query)
        {
            return false;
        }
        
        $sql = "INSERT INTO
                    student(
                        student_id, 
                        last_name, 
                        first_name, 
                        middle_name, 
                        username, 
                        password,
                        grade,
                        section,
                        section_id)
                VALUES";

        $index = 1;
        $highestDataRow = $oSheet->getHighestDataRow();

        try
        {
            foreach($oSheet->getRowIterator($start) as $row)
            {
                $sql .= "(";

                foreach($row->getCellIterator() as $cell)
                {
                    if($cell == '')
                    {
                        return false;
                    } 

                    if($cell->getColumn() == 'F')
                    {
                        $hashed_password = password_hash($cell, PASSWORD_DEFAULT);
                        $sql .= $this->db->escape($hashed_password);
                    } else {
                        $sql .= $this->db->escape($cell);
                    }

                    if($cell->getColumn() != 'H')
                    {
                        $sql .= ",";
                    }
                }

                $sql .= ', ' . $section_id;

                $sql .= ")";
                if($index < $highestDataRow-1)
                {
                    $sql .= ",";
                    $index++;
                }
            }

            $query = $this->db->query($sql);
            if($query)
            {
                return true;
            } return false;
        }
        catch(Exception $e)
        {
            echo "An exception has been caught. " . $e->getMessage();
        }
        
    }

    public function uploadStudentBatchGrades($info)
    {
        $file = FCPATH."uploads\\".$info['file_name'];

        $obj    = PHPExcel_IOFactory::load($file);
        $oSheet = $obj->getActiveSheet();
        $aCells = $obj->getActiveSheet()->getCellCollection();
        $start  = 2;
        
        $sql_array = array(
                        $info['quarter'],
                        $info['section_id']
                        );
        $sql = "DELETE 
                    sg
                    FROM 
                        student_grade  As sg
                    INNER JOIN
                        student As s
                    ON
                        sg.student_id = s.student_id
                WHERE 
                    sg.quarter = ?
                AND
                    s.section_id = ?";
        $query = $this->db->query($sql, $sql_array);

        if(!$query)
        {
            return false;
        }
        
        $sql = "INSERT INTO
                    student_grade(
                        student_id, 
                        subject,
                        grade, 
                        quarter, 
                        faculty_id)
                VALUES";

        $index = 1;
        $highestDataRow = $oSheet->getHighestDataRow();

        try
        {
            foreach($oSheet->getRowIterator($start) as $row)
            {
                $sql .= "(";


                foreach($row->getCellIterator() as $cell)
                {
                    if($cell == '')
                    {
                        return false;
                    } 
                    if($cell->getColumn() !== 'B') {
                        $sql .= $this->db->escape($cell);
                        $sql .= ",";
                    }
                }

                $sql .= $this->db->escape($info['quarter']);
                $sql .= ",";
                $sql .= $this->db->escape($info['faculty_id']);
                $sql .= ")";
                if($index < $highestDataRow-1)
                {
                    $sql .= ",";
                    $index++;
                }
            }

            $query = $this->db->query($sql);

            if($query)
            {
                return true;
            } return false;
        }
        catch(Exception $e)
        {
            echo "An exception has been caught. " . $e->getMessage();
        }
    }

    public function uploadStudentBatchSOA($info)
    {
        $file = FCPATH."uploads\\".$info['file_name'];

        $obj    = PHPExcel_IOFactory::load($file);
        $oSheet = $obj->getActiveSheet();
        $aCells = $obj->getActiveSheet()->getCellCollection();
        $maxCell = $oSheet->getHighestRowAndColumn();
        $data = $oSheet->rangeToArray('A1:' . $maxCell['column'] . $maxCell['row']);
        $row_start  = 3;


        // //@TODO PENDING}
        // $this->db
        //         ->from('student_soa AS soa')
        //         ->join('student AS s', 'soa.student_id = s.student_id')
        //         ->where('');

        // $tables = array('student_soa AS soa', 'student AS s');
        // $this->db->where('soa.quarter', $info['quarter'])
        //          ->where('s.section_id', $info['section_id']);
        // $this->db->delete($tables);

        $this->db->trans_start();

        $deleteExistingSOASql = "UPDATE 
                                    student_soa
                                SET
                                    is_archived = 'y'
                                WHERE
                                    student_id 
                                    IN(";
        // $this->db->query($sql);
        
        $sql = "INSERT INTO
                    student_soa(
                        student_id, 
                        fee,
                        amount, 
                        amount_paid, 
                        due_date,
                        payment_date,
                        remarks
                        )
                VALUES";

        $index = 2;
        $highestDataRow = $oSheet->getHighestDataRow();
        try
        {
            foreach($oSheet->getRowIterator($row_start) as $row)
            {
                $sql .= "(";
                foreach($row->getCellIterator() as $cell)
                {
                    // if(ltrim($cell) == '' && ($cell->getColumn() != 'J' || $cell->getColumn() != 'K'))
                    // {
                    //     // return false;
                    // }

                    if($cell->getColumn() != 'A' && $cell->getColumn() != 'B' && $cell->getColumn() != 'C' && $cell->getColumn() != 'H')
                    {
                        if($cell->getColumn() == 'D') {
                            $deleteExistingSOASql .= $this->db->escape($cell->getValue());
                        }
                        if($cell->getColumn() == 'I' || $cell->getColumn() == 'J')
                        {
                            $sql .= $this->db->escape(PHPExcel_Style_NumberFormat::toFormattedString($cell->getValue(), 'YYYY-MM-DD'));
                        } else {
                            $sql .= $this->db->escape($cell);
                        }

                        if($cell->getColumn() == 'K')
                        {
                            break;
                        }

                        $sql .= ",";
                    }
                }
                $sql .= ")";

                if($index < $highestDataRow-1)
                {
                    $deleteExistingSOASql .= ',';
                    $sql .= ",";
                    $index++;
                }
            }

            $deleteExistingSOASql .= ')';
            $this->db->query($deleteExistingSOASql);
            $query = $this->db->query($sql);

            $this->db->trans_complete();

            if($this->db->trans_status() === FALSE)
            {
                return FALSE;
            } return TRUE;
        }
        catch(Exception $e)
        {
            echo "An exception has been caught. " . $e->getMessage();
        }
    }
}
