<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Faculty_model extends CI_Model
{
    public function getFacultyList()
    {
        $sql = "SELECT
                    *
                FROM
                    faculty
                WHERE
                    is_archived = 0";
        $query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            return $query->result();
        } else 
        {
            return 0;
        }
    }

    public function getSectionList()
    {
        $this->db->select('CONCAT("Grade", grade_level, " - ", name) As grade_section, id', FALSE);
        $this->db->from('section');
        $this->db->order_by('grade_level, name', 'asc');
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        } 
    }

    public function getSectionListAllAndByFaculty($info)
    {
        $this->db->select('se.grade_level, 
                            se.name,
                            se.id,
                            (CASE 
                                WHEN fs.faculty_id = '.$info['faculty_id'].' 
                                THEN "checked" 
                            END) AS checked');
        $this->db->from('section As se');
        $this->db->join('faculty_section As fs', 'se.id = fs.section_id AND fs.faculty_id = '.$info['faculty_id'], 'left');
        $this->db->order_by('checked DESC, grade_level DESC, name ASC');
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        } 
    }

    public function getSectionListByFaculty($info)
    {
        $this->db->select('CONCAT("Grade", grade_level, " - ", name) As grade_section, 
                            se.id,
                            (CASE 
                                WHEN fs.faculty_id = '.$info['faculty_id'].' 
                                THEN "checked" 
                            END) AS checked');
        $this->db->from('faculty As f');
        $this->db->join('faculty_section As fs', 'f.id = fs.faculty_id');
        $this->db->join('section As se', 'fs.section_id = se.id');
        $this->db->order_by('checked, grade_level, name', 'desc');
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        } 
    }

    public function updateSectionListByFaculty($info)
    {
        $sql_array = array();

        $this->db->where('faculty_id', $info['faculty_id']);
        $query = $this->db->delete('faculty_section');

        if(!$query)
        {
            return false;
        }
        foreach($info['section_list'] as $section)
        {
            array_push($sql_array, array(
                                    'faculty_id' => $info['faculty_id'],
                                    'section_id' => $section
                                    ));
        }

        $query = $this->db->insert_batch('faculty_section', $sql_array);

        if($query)
        {
            return true;
        } return false;
    }

    public function getFacultyIdBySectionId($info) {
        $this->db->select('fs.faculty_id');
        $this->db->from('faculty_section As fs');
        $this->db->where('section_id', $info['section_id']);
        $this->db->limit(1);
        $query = $this->db->get();
        
        if($query->num_rows() == 1)
        {
            return $query->result();
        } else {
            return 0;
        }
    }

    public function uploadFacultyBatch($file_name)
    {
        $file = FCPATH."uploads\\".$file_name;

        $obj    = PHPExcel_IOFactory::load($file);
        $oSheet = $obj->getActiveSheet();
        $aCells = $obj->getActiveSheet()->getCellCollection();
        $start  = 2;
        
        $sql = "DELETE FROM faculty WHERE is_admin <> 1";
        $query = $this->db->query($sql);
        if(!$query)
        {
            return false;
        }

        $sql = "INSERT INTO
                    faculty(
                        code, 
                        last_name, 
                        first_name, 
                        middle_name, 
                        username, 
                        password,
                        is_admin)
                VALUES";

        $index = 1;
        $highestDataRow = $oSheet->getHighestDataRow();

        foreach($oSheet->getRowIterator($start) as $row)
        {
            $sql .= "(";

            foreach($row->getCellIterator() as $cell)
            {
                if($cell == '')
                {
                    return false;
                } 

                if($cell->getColumn() == 'F')
                {
                    $hashed_password = password_hash($cell, PASSWORD_DEFAULT);
                    $sql .= $this->db->escape($hashed_password);
                } else {
                    $sql .= $this->db->escape($cell);
                }

                if($cell->getColumn() != 'G')
                {
                    $sql .= ",";
                }
            }

            $sql .= ")";
            if($index < $highestDataRow-1)
            {
                $sql .= ",";
                $index++;
            }
        }

        $query = $this->db->query($sql);
        if($query)
        {
            return true;
        } return $this->db->error();
    }
}
