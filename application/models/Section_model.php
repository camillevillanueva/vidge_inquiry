<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Section_model extends CI_Model
{
    public function addSection($info)
    {
        $sql_array = array(
                        'grade_level' => $info['grade_level'],
                        'name' => $info['section']
                        );
        if(! $this->db->insert('section', $sql_array))
        {
            return false;
        } return true;

    }

    public function deleteSection($info)
    {
        $this->db->set('is_archived', '1', FALSE);
        $this->db->where('id', $info['section_id']);
        $query = $this->db->update('section');
        if ($query)
        {
            return true;
        } return 0;
    }

    public function getSectionList()
    {
        $this->db->select('CONCAT("Grade ", grade_level, " - ", name) AS section,
                            id,
                            grade_level,
                            name');
        $this->db->from('section');
        $this->db->where('is_archived', '0');
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result();
        } return 0;
    }

    public function getSectionListByTeacher($teacher_id)
    {
        $sql = "SELECT
                    CONCAT('Grade ', grade_level, ' - ', se.name) As section,
                    se.id
                FROM
                    faculty_section As fs
                INNER JOIN
                    section As se
                ON
                    fs.section_id = se.id
                WHERE
                    fs.faculty_id = ?
                AND
                    se.is_archived = 0";
        $query = $this->db->query($sql, $teacher_id);

        if($query->num_rows() > 0)
        {
            return $query->result();
        } else 
        {
            return 0;
        }
    }

    public function updateSection($info)
    {
        $this->db->set('name', $info['section_name']);
        $this->db->where('id', $info['section_id']);
        $query = $this->db->update('section');

        if($query)
        {
            return true;
        } return false;
    }
}
