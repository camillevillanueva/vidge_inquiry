-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: vidge_inquiry
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `advisory`
--

DROP TABLE IF EXISTS `advisory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advisory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` varchar(100) NOT NULL,
  `message` varchar(300) NOT NULL,
  `published_date` datetime DEFAULT NULL,
  `recipient_type` enum('all','grade','section','student') DEFAULT NULL,
  `student_id` int(11) NOT NULL COMMENT 'IF recipient_type = ''student''',
  `section_id` int(11) DEFAULT NULL COMMENT 'IF recipient_type = section''',
  `grade_level` int(11) DEFAULT NULL COMMENT 'IF recipient_type = ''grade''',
  `title` varchar(50) NOT NULL,
  `is_archived` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advisory`
--

LOCK TABLES `advisory` WRITE;
/*!40000 ALTER TABLE `advisory` DISABLE KEYS */;
INSERT INTO `advisory` VALUES (30,'0649','Hello','2017-10-26 22:02:25',NULL,0,NULL,NULL,'Hello World',0),(31,'0649','Coffee','2017-10-29 17:06:32',NULL,0,NULL,NULL,'Hello World',1),(32,'0000','Hello World','2018-01-15 15:41:32',NULL,0,NULL,NULL,'Testing ',0);
/*!40000 ALTER TABLE `advisory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advisory_recipient`
--

DROP TABLE IF EXISTS `advisory_recipient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advisory_recipient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `advisory_id` int(11) NOT NULL,
  `student_id` varchar(11) NOT NULL,
  `is_read` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advisory_recipient`
--

LOCK TABLES `advisory_recipient` WRITE;
/*!40000 ALTER TABLE `advisory_recipient` DISABLE KEYS */;
INSERT INTO `advisory_recipient` VALUES (5,29,'2013-0211',0),(6,29,'2013-0212',0),(7,30,'2013-0211',0),(8,30,'2013-0212',1),(9,31,'2013-0212',1),(10,32,'2013-0212',0),(11,32,'2013-0215',0);
/*!40000 ALTER TABLE `advisory_recipient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faculty`
--

DROP TABLE IF EXISTS `faculty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faculty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `middle_name` varchar(200) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` longtext NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `code` varchar(45) NOT NULL,
  `is_archived` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faculty`
--

LOCK TABLES `faculty` WRITE;
/*!40000 ALTER TABLE `faculty` DISABLE KEYS */;
INSERT INTO `faculty` VALUES (1,'Camille','Villanueva','Galang','Cams','$2y$10$/Lwp51uX6YnKXC/MxVLnBOL3duvfSYHtorIPhgXZB8JbNLsVAeUKK',1,'0000',0),(16,'Camille','Villanueva','Galang','cgvillanueva','$2y$10$/Lwp51uX6YnKXC/MxVLnBOL3duvfSYHtorIPhgXZB8JbNLsVAeUKK',0,'0649',0),(17,'Buena','Villanueva','Galang','bgvillanueva','$2y$10$QIHmol4ea7ILloomctc27eNohfQhSxeBsOHRoGGGVEpwjSdKGUFcS',0,'0650',0);
/*!40000 ALTER TABLE `faculty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faculty_section`
--

DROP TABLE IF EXISTS `faculty_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faculty_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faculty_id` varchar(45) NOT NULL,
  `section_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faculty_section`
--

LOCK TABLES `faculty_section` WRITE;
/*!40000 ALTER TABLE `faculty_section` DISABLE KEYS */;
INSERT INTO `faculty_section` VALUES (13,'0650',1),(14,'0650',2),(15,'0650',3),(16,'0650',4),(19,'0649',1),(20,'0649',3),(21,'0649',2),(22,'17',2);
/*!40000 ALTER TABLE `faculty_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_level` int(11) NOT NULL DEFAULT '1',
  `name` varchar(45) NOT NULL,
  `is_archived` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section`
--

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
INSERT INTO `section` VALUES (1,6,'Tuna',0),(2,5,'Strawberry',0),(3,5,'Ham',0),(4,5,'Sausage',0);
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` longtext NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `middle_name` varchar(200) DEFAULT NULL,
  `student_id` varchar(45) NOT NULL,
  `section_id` int(11) NOT NULL,
  `is_archived` tinyint(4) NOT NULL,
  `grade` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (7,'camillegv','$2y$10$3gbgab/uIkEzM420ahHHaOSn58yZ.V5QlmKUAGxWN6kF1vzqKmita','Camille','Villanueva','Galang','2013-0212',1,0,6,'Tuna'),(8,'gandalf','$2y$10$1STM9K4Cv2cLvl3YNymL6.hdyNQTeNnjPU2HLdcmTI3v3RtVQceZW','Gandalf','Wizards','Plato','2013-0215',1,0,6,'Tuna');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_grade`
--

DROP TABLE IF EXISTS `student_grade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(45) NOT NULL,
  `grade` decimal(10,4) NOT NULL,
  `quarter` int(11) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `upload_date` datetime DEFAULT NULL,
  `subject` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_grade`
--

LOCK TABLES `student_grade` WRITE;
/*!40000 ALTER TABLE `student_grade` DISABLE KEYS */;
INSERT INTO `student_grade` VALUES (7,'2013-0212',92.0000,1,16,'2017-10-23 00:48:30','Filipino'),(8,'2013-0212',94.0000,3,16,'2017-10-23 00:49:03','Filipino'),(11,'2013',90.0000,1,16,'2017-10-23 00:53:29','Filipino'),(13,'2013-0212',85.0000,2,16,'2017-10-23 01:02:38','English'),(14,'2',89.0000,1,16,'2017-10-23 02:03:11','Math'),(16,'2',93.0000,2,16,'2017-10-26 18:07:07','Math');
/*!40000 ALTER TABLE `student_grade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_soa`
--

DROP TABLE IF EXISTS `student_soa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_soa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(45) NOT NULL,
  `fee` varchar(100) DEFAULT '0.0',
  `amount` decimal(10,2) NOT NULL,
  `upload_date` datetime DEFAULT '0000-00-00 00:00:00',
  `amount_paid` decimal(10,2) DEFAULT '0.00',
  `due_date` datetime DEFAULT '0000-00-00 00:00:00',
  `payment_date` datetime DEFAULT '0000-00-00 00:00:00',
  `remarks` varchar(150) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_soa`
--

LOCK TABLES `student_soa` WRITE;
/*!40000 ALTER TABLE `student_soa` DISABLE KEYS */;
INSERT INTO `student_soa` VALUES (44,'2013-0212','Tuition',1500.00,'2017-10-29 14:48:15',500.00,'2017-10-26 00:00:00','2017-10-18 00:00:00','Remarks'),(45,'2013-0212','Energy',500.00,'2017-10-29 14:48:15',500.00,'2017-10-19 00:00:00','2017-10-18 00:00:00','Remarks'),(46,'2013-0212','Misc',500.00,'2017-10-29 14:48:15',500.00,'2017-10-12 00:00:00','2017-10-18 00:00:00','Remarks'),(47,'5','Tuition Fee',5000.00,'2018-01-15 15:43:11',500.00,'2017-10-12 00:00:00',NULL,'rems'),(48,'5','Energy - June',2000.00,'2018-01-15 15:43:11',0.00,'2017-12-15 00:00:00',NULL,'text'),(49,'5','Energy - July',2000.00,'2018-01-15 15:43:11',500.00,'2017-10-05 00:00:00',NULL,''),(50,'2','Mishka',3500.00,'2018-01-15 15:43:11',3500.00,'2017-10-18 00:00:00',NULL,'World'),(51,'2','Milo',50.00,'2018-01-15 15:43:11',50.00,'2017-10-12 00:00:00','2017-12-12 00:00:00','Hello'),(52,'2013-0212','Tuition Fee',5000.00,'2018-01-15 15:47:53',500.00,'2017-10-12 00:00:00',NULL,'rems'),(53,'2013-0212','Energy - June',2000.00,'2018-01-15 15:47:53',0.00,'2017-12-15 00:00:00',NULL,'text'),(54,'5','Energy - July',2000.00,'2018-01-15 15:47:53',500.00,'2017-10-05 00:00:00',NULL,''),(55,'2','Mishka',3500.00,'2018-01-15 15:47:53',3500.00,'2017-10-18 00:00:00',NULL,'World'),(56,'2','Milo',50.00,'2018-01-15 15:47:53',50.00,'2017-10-12 00:00:00','2017-12-12 00:00:00','Hello');
/*!40000 ALTER TABLE `student_soa` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-15 16:00:03
