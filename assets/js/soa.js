$(document).ready(function()
{
    $('.tbl-student-soa').DataTable();

   $('.tbl-student-soa').on('click', 'tr', function()
    {
        var student_id = $(this).attr('id');
        $.ajax({
            type: 'get',
            url: base_url + 'student/getStudentSOA/' + student_id,
            success: function(msg)
            {
                if(msg != 'null')
                {
                    var soa_items = JSON.parse(msg);
                    var soa = '';
                    // <th>Fee</th>
                    //                     <th>Amount Due</th>
                    //                     <th>Due Date</th>
                    //                     <th>Amount Paid</th>
                    //                     <th>Payment Date</th>
                    //                     <th>Balance</th>
                    //                     <th>Remarks</th>
                    $('#student-id').html('');
                    $('#student-full-name').html('');
                    $('#student-grade-section').html('');

                    $('#student-id').append(soa_items[0].student_id);
                    $('#student-full-name').append(soa_items[0].full_name);
                    $('#student-grade-section').append(soa_items[0].grade_section);

                    for(var i = 0; i < soa_items.length; ++i)
                    {
                        soa += '<tr>';
                        soa += '<td>' + soa_items[i].fee + '</td>';
                        soa += '<td>' + soa_items[i].amount + '</td>';
                        soa += '<td>' + soa_items[i].due_date + '</td>';
                        soa += '<td>' + soa_items[i].amount_paid + '</td>';
                        soa += '<td>' + soa_items[i].payment_date + '</td>';
                        soa += '<td>' + parseFloat(parseFloat(soa_items[i].amount) - parseFloat(soa_items[i].amount_paid)).toFixed(2) + '</td>';
                        soa += '<td>' + soa_items[i].remarks + '</td>';
                        soa += '</tr>';
                    }

                    $('.tbl-modal-student-soa tbody').html('');
                    $('.tbl-modal-student-soa tbody').append(soa);
                    $('.modal-student-soa').modal('show');
                } else {
                    alert('No records found');
                }
            },
            error: function(jqXHR)
            {
                alert('Something went wrong. Try again.');
                console.log(jqXHR);
            }
        });
    });
});

$(document).ajaxComplete(function(event, xhr, settings) {
    
});
