$(document).ready(function()
{
    var table = $('.tbl-advisory-recipients').DataTable();;
    $('.tbl-advisory-list').DataTable();

    $('.btn-delete-advisory').click(function()
    {
        var advisory_id = $(this).attr('id');

        $.ajax({
            type: 'post',
            url: base_url + 'advisory/deleteAdvisory',
            data: {
                'advisory_id' : advisory_id
            },
            success: function(msg)
            {
                if(msg != '0')
                {
                    alert('Advisory deleted');
                } else {
                    alert('Failed to delete advisory');
                }
            },
            error: function(jqXHR)
            {
                alert('Something went wrong. Try again.');
                console.log(jqXHR);
            }
        });
        window.location.href='';
    });
    $('#div-advisory-message').change(function()
    {
        $("#textarea-advisory-message").val($("#div-advisory-message").html());
    })

    // Handle click on "Select all" control
   $('#select-all-recipients').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({'search':'applied'}).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   $('.tbl-advisory-list').on('click', 'tr', function()
    {
        var current_advisory = $(this).attr('id');
        $.ajax({
            type: 'get',
            url: base_url + 'Advisory/getAdvisoryInformation/' + current_advisory,
            success: function(msg)
            {
                if(msg != '[]')
                {
                    var advisory = JSON.parse(msg);
                    var a = '';
                    
                    $('#advisory-title').html('');
                    $('#advisory-title').html(advisory[0].title);

                    $('#advisory-message').html('');
                    $('#advisory-message').html(advisory[0].message);

                    $('#advisory-published-date').html('');
                    $('#advisory-published-date').html(advisory[0].published_date);
                    
                    for(var i = 0; i < advisory.length; ++i)
                    {
                        a += advisory[i].recipient;

                        if(i < advisory.length-1)
                        {
                            a += ', ';
                        }
                    }

                    $('#advisory-recipients').html('');
                    $('#advisory-recipients').html(a);

                    $('.btn-delete-advisory').attr('id', current_advisory);
                } else {
                    alert('No records found');
                }
            },
            error: function(jqXHR)
            {
                alert('Something went wrong. Try again.');
                console.log(jqXHR);
            }
        });
        $('.modal-advisory-information').modal('show');
    });

   $('.modal-advisory-information').on('hidden.bs.modal', function () {
     location.reload();
    });

   // Handle click on checkbox to set state of "Select all" control
   $('.tbl-advisory-recipients tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('.tbl-advisory-recipients').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });
});

$(document).ajaxComplete(function(event, xhr, settings) {
    
});
