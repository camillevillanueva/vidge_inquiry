$(document).ready(function()
{  
   $('.faculty-login-form').submit(function(e) {
        e.preventDefault();
        var username = $('#form-username').val();
        var password = $('#form-password').val();

        $.ajax({
            type: 'post',
            url: base_url + 'loginf',
            data: {
                'form-username' : username,
                'form-password' : password
            },
            success: function(m)
            {
                if(m === 'failed')
                {
                    alert('Invalid username and password combination. Try again.');
                    window.location.href="";
                } else {
                    console.log(m);
                    window.location.href = base_url + 'home';
                }
            }
        });

   });

   $('.student-login-form').submit(function(e) {
        e.preventDefault();
        var username = $('#form-username').val();
        var password = $('#form-password').val();

        $.ajax({
            type: 'post',
            url: base_url + 'logins',
            data: {
                'form-username' : username,
                'form-password' : password
            },
            success: function(m)
            {
                if(m === 'failed')
                {
                    alert('Invalid username and password combination. Try again.');
                    window.location.href="";
                } else {
                        window.location.href=base_url + 'home';
                }
            }
        });

   });
});
