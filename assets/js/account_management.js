$(document).ready(function()
{
    /** INITIALIZE DATATABLES **/
    $('.tbl-faculty').DataTable();
    $('.tbl-student').DataTable();
    /**END OF INITIALIZING DATATABLES **/

    $('.deactivate').on('click', function()
    {
        if(confirm('Are you sure you want to deactivate this account?'))
        {
            var id = $(this).attr('id');
            var type = $(this).data('type');

            $.ajax({
                type: 'post',
                url: base_url + 'user/deactivate/' + type + '/' + id,
                success: function(msg)
                {
                    if(msg != '0')
                    {
                        alert('Account Deactivated');
                    } else {
                        alert('Deactivating failed. Try again.');
                    }
                    window.location.href='';
                },
                error: function(jqXHR)
                {
                    alert('Deactivating failed. Something went wrong. Try again.');
                    console.log(jqXHR);
                }
            });
        } else {
            return;
        }
    });

    $('.faculty').on('click', function()
    {
        var faculty_id = $(this).attr('id');
        $.ajax({
            type: 'get',
            url: base_url + 'faculty/getSectionListAllAndByFaculty',
            data: {
                'faculty_id' : faculty_id
            },
            success: function(msg)
            {
                if(msg != 'null')
                {
                    var section = JSON.parse(msg);
                    var s = '';

                    for(var i = 0; i < section.length; i++)   
                    {
                        s += '<tr ';
                        if(section[i].checked == 'checked')
                        {
                            s += 'style="background-color: #1ABB9C; color: #fff"';
                        }
                        s += '>';
                        s += '<td><input type="checkbox" ';
                        if(section[i].checked == 'checked')
                        {
                            s += 'checked';
                        }
                        s += ' id='+section[i].id +'></td>';
                        s += '<td>' + section[i].grade_level + '</td>';
                        s += '<td>' + section[i].name + '</td>';
                        s += '</tr>';
                    }
                    
                    $('.update-faculty-sections').attr('id', faculty_id);
                    $('.tbl-sections tbody').html('');
                    $('.tbl-sections tbody').append(s);
                    $('.modal-faculty-handled-sections').modal('show');
                } else {
                    alert('0 records found.');
                }
            },
            error: function(jqXHR)
            {
                alert('Something went wrong. Try again.');
                console.log(jqXHR);
            }
        });
    });

    $('.update-faculty-sections').click(function()
    {
        var checked_sections = [];
        var faculty_id = $(this).attr('id');

        $('.tbl-sections tbody').find('input[type="checkbox"]:checked').each(function(index, element)
        {
            checked_sections.push($(this).attr('id'));
        });

        $.ajax({
            type: 'post',
            url: base_url + 'faculty/updateSectionListByFaculty',
            data: {
                'faculty_id' : faculty_id,
                'section_list' : JSON.stringify(checked_sections) 
            },
            success: function(msg)
            {
                if(msg != '0')
                {
                    alert('Updating successful.');
                } else {
                    alert('Updating failed.');
                    console.log(msg);
                }
            },
            error:function(jqXHR)
            {
                alert('Updating failed.');
                console.log(jqXHR);
            }
        });
        window.location.href='';
    });
});

$(document).ajaxComplete(function(event, xhr, settings) {
    
});
