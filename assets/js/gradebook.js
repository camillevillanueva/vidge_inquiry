$(document).ready(function()
{
    $('.tbl-student-grades').DataTable();

    //Initialize faculty_id for uploading
    var section_id = parseInt($('.section-list').val()); 
    $.ajax({
        type: 'get',
        url: base_url + 'faculty/getFacultyIdBySectionId',
        data: {
            'section_id' : section_id
        },
        success: function(msg) {
            if (msg != '0')
            {
                $('input[name=faculty_id]').val(JSON.parse(msg).faculty_id);
            } else {
                alert(msg + ' records found. ');
            }
        },
        error: function(jqXHR, textStatus, responseText) {

        }
    });

    $('.tbl-student-grades').on('click', 'tr', function()
    {
        var id = $(this).attr('id');

        $.ajax({
            type: 'get',
            url: base_url + 'student/getStudentGrades',
            data: {
                'student_id' : id
            },
            success: function(msg)
            {
                if (msg != '0')
                {
                    $('.tbl-modal-student-grades tbody').html('');
                    $('#student-id').html('');
                    $('#student-full-name').html('');
                    $('#student-grade-section').html('');

                    var grades = JSON.parse(msg);
                    $('#student-id').append(grades[0].student_id);
                    $('#student-full-name').append(grades[0].full_name);
                    $('#student-grade-section').append(grades[0].grade_section);

                    var g = '';
                    
                    for(var j = 0; j < grades.length; j++)
                    {
                        g += '<tr>';
                        g += '<td>' + grades[j].subject + '</td>';
                        g += '<td>' + grades[j].Q1 + '</td>';
                        g += '<td>' + grades[j].Q2 + '</td>';
                        g += '<td>' + grades[j].Q3 + '</td>';
                        g += '<td>' + grades[j].Q4 + '</td>';
                        g += '</tr>';
                    }
                    

                    $('.tbl-modal-student-grades tbody').append(g);
                    $('.modal-student-grades').modal('show');
                } else {
                    alert(msg + ' records found. ');
                }
            },
            error: function(jqXHR, textStatus, responseText)
            {
                alert('Something went wrong! Try again. ')
                console.log(jqXHR);
                console.log(textStatus);
                console.log(responseText);
            }
        });
    });

    $('select[name=section-list]').change(function(){
        var section_id = parseInt($('.section-list').val());
        
        $.ajax({
            type: 'get',
            url: base_url + 'faculty/getFacultyIdBySectionId',
            data: {
                'section_id' : section_id
            },
            success: function(msg) {
                if (msg != '0')
                {
                    $('input[name=faculty_id]').val(JSON.parse(msg).faculty_id);
                } else {
                    alert(msg + ' records found. ');
                }
            },
            error: function(jqXHR, textStatus, responseText) {

            }
        });
    });
});

$(document).ajaxComplete(function(event, xhr, settings) {
    
});
