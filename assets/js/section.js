$(document).ready(function()
{
    $('.tbl-sections').DataTable();

    $('.tbl-sections').on('click', '.delete-section', function()
    {
        if(confirm('Are you sure you want to archive this section?'))
        {
            var id = $(this).attr('id');

            $.ajax({
                type: 'post',
                url: base_url + 'section/deleteSection/' + id,
                success: function(msg)
                {
                    if(msg != '0')
                    {
                        alert('Section Archived');
                    } else {
                        alert('Archiving failed. Try again.');
                    }
                    window.location.href='';
                },
                error: function(jqXHR)
                {
                    alert('Archiving failed. Something went wrong. Try again.');
                    console.log(jqXHR);
                }
            });
        } else {
            return;
        }
    });

    $('.tbl-sections').on('click', 'tr', function()
    {
        var section_name = $(this).find('.section-name').html();
        var section_id = $(this).attr('id');
        var grade_level = $(this).find('.grade-level').html();

        $('#form-modal-section-list #section-name').val(section_name);
        $('#form-modal-section-list #grade-level').val(grade_level);
        $('.update-section').attr('id', section_id);
        $('.modal-section-list').modal('show');
    });

    $('#form-modal-section-list').submit(function(e)
    {
        e.preventDefault();
        var section_name = $('#section-name').val();
        var section_id = $('.update-section').attr('id');
        var grade_level = $('#grade-level').val();
        
        $.ajax({
            type: 'post',
            url: base_url + 'section/updateSection',
            data: {
                'section_id' : section_id,
                'section_name' : section_name,
                'grade_level' : grade_level
            },
            success: function(msg)
            {
                if(msg != '0')
                {
                    alert('Updating success');
                } else {
                    alert('Updating failed');
                }
            },
            error: function(jqXHR)
            {
                alert('Something went wrong! Try again.');
                console.log(jqXHR);
            }
        });

        window.location.href='';
    });
});

$(document).ajaxComplete(function(event, xhr, settings) {
    
});
